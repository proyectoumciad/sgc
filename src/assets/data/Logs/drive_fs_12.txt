-------------------------------------
drive_fs log started
version: 33.0.16.0
-------------------------------------
2019-10-17T13:11:42.773ZI [4500] instrumentation.cc:107:OpenLogFileInDirectory Logging to C:\Users\usuario\AppData\Local\Google\DriveFS\Logs\drive_fs.txt
2019-10-17T13:11:42.774ZI [6028] drive_fs_main.cc:105:LogExistingCrashReports No existing crash reports found.
2019-10-17T13:12:28.322ZI [6028] drive_fs.cc:317:RunDriveFS Options: argv_0: "C:\\Program Files\\Google\\Drive File Stream\\33.0.16.0\\GoogleDriveFS.exe"
base_path: "C:\\Users\\usuario\\AppData\\Local\\Google\\DriveFS"
ipc_pipe_path: "\\\\.\\Pipe\\GoogleDriveFSPipe_usuario"
shell_ipc_pipe_path: "\\\\.\\Pipe\\GoogleDriveFSPipe_usuario_shell"
crash_handler_token: "\\\\.\\pipe\\crashpad_5736_MPVCKQWWWDWBJFEI"
feature_config {
  enabled: true
  virtual_folders: true
  thumbnails: true
  deprecated: false
  drive_dot: true
  metadata_cache_reset_counter: 0
  spotlight: true
  feedback: true
  share_dialog: true
  shell_ipc: true
  changelog_download_throttle_time_ms: 16000
  temporary_items_virtual_folder: true
  local_disk_aware_get_free_space: true
  context_menu_copy_link: true
  dokan_keep_alive_timeout_ms: 1400000000
  dokan_keep_alive_timeout_after_wakeup_ms: 1400000000
  pause_syncing_option: true
  bandwidth_throttling: true
  attach_logs_to_feedback_option: true
  max_parallel_background_cloud_queries: 3
  structured_logging: true
  drive_dot_ui: true
  relocatable_content_cache: true
  onboarding: true
  content_cache_max_kbytes: 1000000000
  block_mac_antivirus: true
  full_trash_folder_support: true
  context_menu_force_refresh_folder: true
  enable_kernel_based_change_replayer: true
  preferences_dialog: true
  sync_client_migration: true
  content_download_http_416_workaround: true
  metadata_db_vacuum: false
  crash_on_failed_thread_checker_assert: false
  mount_point_changed_dialog: true
  allow_windows_system_user_access: false
  persist_do_not_show_again_preferences: true
  do_not_fake_operation_success: true
  status_window_click_show_file_in_file_manager: true
  use_osxfuse_read_blacklist: false
  dokan_oplocks: true
  context_menu_copy_link_notification: true
  max_parallel_downloads_per_file: 5
  two_level_folder_prefetch: false
  max_parallel_push_task_instances: 3
  emm_support: true
  use_file_organizer_capabilities: true
  generate_diagnostic_info: true
  fetch_content_bytes_before_cancel_percent_multiplier: 500
  gui_bandwidth_throttling_control: false
  local_machine_override_preferences: true
  allow_undo_move: true
  outlook_addin: true
  autostart_preference: true
  gui_autostart_option: true
  mojave_overlays_fix: false
  curl_proxy_authentication: true
  switchblade: true
  notification_drawer: false
  gui_reset_preferences_menu_option: true
  cancel_upload_option: false
  quicklook_thumbnails: false
  min_bytes_saved_to_split_request: 0
  reset_account_after_fatal_local_store_failure: true
  detect_stable_id_reuse: true
  force_refresh_folder_normal_menu: false
  supply_json_gdoc_content: false
  structured_log_max_file_size_kbytes: 2048
  dokan_dispatch_monitor_check_ms: 0
  dokan_dispatch_monitor_warn_ms: 0
  non_blocking_notifications: false
  sqlite_normal_transaction_sync: false
  unlocked_mac_getxattr: false
  inform_users_of_pending_uploads: true
  finder_process_names: "DesktopServicesHelper"
  rename_team_drive: true
  show_rename_td_notification: true
  reset_account_after_local_largest_change_id_corrupt: false
  emm_secondary_identifier: false
  28: 1
}
feature_config_override {
}
enable_tracing: false
enable_ui: true
enable_file_dances: true
use_fake_cello_fs: false
drive_api_options {
  apiary_trace_token: ""
  apiary_trace_regex: ""
  inet_family: IPV4_ONLY
  dapper_trace_regex: ""
}
enable_poll_for_changes: true
drive_create_options {
  use_fake_cloud_store: false
  use_console_auth: false
  authorize_new_user: false
  refresh_token: ""
  user_email: ""
  token_uri: ""
  auth_uri: ""
}
enable_field_event_recording: true
open_gdoc_path: ""
started_by_installer: false
locale: ""
force_onboarding: false
log_connection_details: false
cmdline_preferences {
}
core_string_resources {
  my_drive: "Mi unidad"
  team_drives: "Unidades compartidas"
  computers: "Ordenadores"
}
use_intranet_connectivity_to_check_online: false
crash_handler_init_status: SUCCESS
sync_roots_file_path: ""
open_gdocs_root: false
open_gsheets_root: false
open_gslides_root: false
crash_on_core_start: false
use_curl_logging_scribe: false
mirror_sync: false
slurp_sync: false
force_case_sensitivity: false

2019-10-17T13:12:28.322ZI [6028] drive_fs.cc:184:LogSystemConfig OS: Windows/10.0.10586
2019-10-17T13:12:28.323ZI [6028] drive_fs.cc:191:LogSystemConfig Architecture: 64-bit
2019-10-17T13:12:28.323ZI [6028] drive_fs.cc:200:LogSystemConfig Total RAM: 15 GiB
2019-10-17T13:12:28.323ZI [6028] drive_fs.cc:216:LogSystemConfig Disk space: 82.03 GB free / 313.03 GB total
2019-10-17T13:12:28.324ZI [6028] drive_fs.cc:232:LogSystemConfig Timezone: UTC-0500 (Hora est. Pac�fico, Sudam�rica)
2019-10-17T13:12:28.326ZI [6028] client.cc:2160:LoadPreferencesFromDisk preferences: {
local_machine_defaults {
}
local_user {
  content_cache_base_path: "C:\\Users\\usuario\\AppData\\Local\\Google\\file"
}
local_machine_override {
}
combined_preferences {
  content_cache_base_path: "C:\\Users\\usuario\\AppData\\Local\\Google\\file"
}
}
2019-10-17T13:12:28.326ZI [6028] client.cc:2162:LoadPreferencesFromDisk effective_preferences: {
mount_point_path: "G"
content_cache_base_path: "C:\\Users\\usuario\\AppData\\Local\\Google\\file"
trusted_root_certs_file_path: "C:\\Program Files\\Google\\Drive File Stream\\33.0.16.0\\config\\roots.pem"
disable_ssl_validation: false
disable_crl_check: false
direct_connection: false
force_browser_auth: false
disable_realtime_presence: false
do_not_show_dialogs {
  mount_point_changed: false
  confirm_shared_folder_move_in: false
  confirm_shared_folder_move_out: false
  confirm_td_file_move_out: false
  confirm_move_to_untrusted_td: false
  warning_move_to_untrusted_td: false
  warning_td_file_move_out: false
  warning_shared_folder_move_in: false
  warning_shared_folder_move_out: false
}
autostart_on_login: true
outlook_attachment_size_threshold_mbytes: 10
disable_outlook_plugin: false
}
2019-10-17T13:12:28.327ZI [6028] ipc_socket_win.cc:264:GetNextConnection Accepting next connection
2019-10-17T13:12:28.899ZI [6028] app.cc:450:RegisterOutlookAddIn Outlook add-in already registered.
2019-10-17T13:12:28.901ZI [6028] app.cc:897:GetKnownFolderPath Path doesn't exist: 'C:\Users\usuario\Desktop\Google Drive.lnk'
2019-10-17T13:12:28.901ZI [6028] app.cc:960:UpdateIconGoogleSyncShortcut Could not update shortcut icon. Error getting path to link.
2019-10-17T13:12:28.902ZI [6028] app.cc:897:GetKnownFolderPath Path doesn't exist: 'C:\Users\usuario\Google Drive'
2019-10-17T13:12:28.902ZI [6028] app.cc:929:CleanUpGoogleSyncQuickAccess Could not clean up Quick Access icon. Error getting path.
2019-10-17T13:12:34.509ZI [6868] credential_store.cc:55:InitCredential Credential initUser: 
2019-10-17T13:12:34.509ZI [6868] drive_cloud_store.cc:184:CreateDriveCloudStoreAsync Using Drive API v2
2019-10-17T13:12:34.509ZI [6868] curl_api.cc:1272:Create SSL verification with CAcerts path: C:\Program Files\Google\Drive File Stream\33.0.16.0\config\roots.pem
2019-10-17T13:12:34.510ZI [6868] credential_store.cc:55:InitCredential Credential initUser: 
2019-10-17T13:12:34.534ZI [6872] network_status_listener_win.cc:215:CheckConnection Checking connection state.
2019-10-17T13:12:34.536ZI [6872] network_status_listener_win.cc:244:CheckConnection This adapter is online.
2019-10-17T13:12:34.537ZI [6872] network_status_listener_win.cc:255:CheckConnection Result: online
2019-10-17T13:12:34.537ZI [6872] network_status_listener_win.cc:141:UpdateState Notifying of change to state 2
2019-10-17T13:12:49.536ZI [6028] chrome_webview.cc:299:OnLoadStart Loading https://accounts.google.com/signin/oauth/cl?hl=es&client_id=<Redacted>                                                              &as=F15JLbpTny9-L5wgPemYUA&approval_state=!ChR4Y18yMW9YY1hJbWJ5cGZfSFp3OBIfazVpTEkwbzg3Q0FVVUEyYUd3VDlRSnhRSEpTZDNSWQ%E2%88%99AJDr988AAAAAXam6RSqxJEBwJLnWp640SiSOEX9Ra8e3&oauthgdpr=1&oauthriskyscope=1&xsrfsig=ChkAeAh8T1fqXI_4pSB5k584peVurSqqIfJtEg5hcHByb3ZhbF9zdGF0ZRILZGVzdGluYXRpb24SBXNvYWN1Eg9vYXV0aHJpc2t5c2NvcGU
2019-10-17T13:13:01.438ZI [6028] chrome_webview.cc:402:OnQuery DOM onReady fired.
2019-10-17T13:13:04.867ZI [5168] chrome_webview.cc:267:OnBeforeResourceLoad User-Agent: 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36'
2019-10-17T13:15:47.307ZE [6028] client.cc:509:operator() Error getting auth token: CANCELLED
2019-10-17T13:15:47.308ZI [6028] core_controller.cc:344:StopCore StopCore called from client.cc:1444:operator()
2019-10-17T13:15:47.308ZI [6868] core_controller.cc:82:MaybeUpdateReturnCode Updating return code to CELLOFS_INIT_INTERRUPTED
2019-10-17T13:15:51.003ZE [6028] native_window_win.cc:205:OnPaint GetWindowRgn failed
2019-10-17T13:15:54.618ZI [6028] client.cc:1140:Quit Quit initiated from status_dialog.cc:296:OnQuery
2019-10-17T13:15:54.618ZI [6028] client.cc:1158:SilentQuit Silent quit initiated from client.cc:1151:operator()
2019-10-17T13:15:54.618ZI [6028] core_controller.cc:344:StopCore StopCore called from client.cc:1159:SilentQuit
2019-10-17T13:15:54.629ZI [6028] core_controller.cc:344:StopCore StopCore called from core_controller.cc:278:Shutdown
2019-10-17T13:15:54.629ZI [6028] drive_fs.cc:417:RunDriveFS Return code: CELLOFS_INIT_INTERRUPTED
2019-10-17T13:15:54.645ZI [4704] instrumentation.cc:498:operator() Writing Chrome Trace to C:\Users\usuario\AppData\Local\Google\DriveFS\Logs\startup_trace_2019-10-17T13_15_54.json
2019-10-17T13:15:54.646ZI [4704] instrumentation.cc:500:operator() Trace successfully written
